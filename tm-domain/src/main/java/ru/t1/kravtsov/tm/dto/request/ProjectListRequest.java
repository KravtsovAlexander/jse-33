package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.enumerated.Sort;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest(final @Nullable String token) {
        super(token);
    }

}
