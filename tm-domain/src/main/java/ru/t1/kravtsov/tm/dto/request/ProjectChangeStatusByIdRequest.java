package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.enumerated.Status;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(final @Nullable String token) {
        super(token);
    }

}
