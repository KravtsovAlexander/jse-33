package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Display task by project id.";

    @NotNull
    public static final String NAME = "task-list-by-project-id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final List<Task> tasks = getTaskEndpoint()
                .listTaskByProjectId(request)
                .getTasks();
        if (tasks != null) {
            renderTasks(tasks);
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
