package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.api.service.ISessionService;
import ru.t1.kravtsov.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(final @NotNull ISessionRepository repository) {
        super(repository);
    }

}
